<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique()->sentence(); // Para el titulo del post

        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'extract' => $this->faker->text(250), //Expecifico que el texto sea de 250 caracteres
            'body' => $this->faker->text(2000), //Expecifico que el texto sea de 250 caracteres
            'status' => $this->faker->randomElement([1,2]),
            'category_id' => Category::all()->random()->id, // Llamo al modelo Category, traigo todos los registros elijo uno al azar y pido su id.
            'user_id' => User::all()->random()->id // Llamo al modelo User, traigo todos los registros elijo uno al azar y pido su id.
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Enrique Saborit Crespo',
            'email' => 'esaborit@gmail.com',
            'password' => bcrypt('esaborit')
        ])->assignRole('Admin');
        
        User::factory(9)->create();
    }
}

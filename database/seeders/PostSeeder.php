<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::factory(300)->create();

        foreach ($posts as $post) {
            // añado una imagen a cada post (como el ImageFactory solo añade la url el resto lo añado aqui)
            Image::factory(1)->create([
                'imageable_id' => $post->id,
                'imageable_type' => Post::class
            ]);
            // Añado 2 etiquetas a cada post
            $post->tags()->attach([ // attach() rellena la tabla post_tag
                rand(1, 4),
                rand(5, 8)
            ]);
        }
    }
}

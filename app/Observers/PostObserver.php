<?php

namespace App\Observers;

use App\Models\Post;
use Illuminate\Support\Facades\Storage;

class PostObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function creating(Post $post)
    {
        if(! \App::runningInConsole()){ // Para que no se ejecute cuando se esten ejecutando los seeders desde la consola pues no hay un usuario autenticado.
            $post->user_id = auth()->user()->id; //cada vez que se cree un post se asigna l campo user_id el valor del usuario autentiado.
        }
    }


    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleting(Post $post) //cada vez que se elimina un post si este tiene imagen la borramos tambien.
    {
        if($post->image){
            Storage::delete($post->image->url);
        }
    }

  
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage; //Para poder almacenar las imagenes a la carpeta public del servidor

use function GuzzleHttp\Promise\all;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id'); //Devuelve un array de parejas clave=>valor ('id'=>'nombre')
        $tags = Tag::all();//Devuelve un array de objetos Tag.
        
        return view('admin.posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StorePostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = Post::create($request->all());
        
        if ($request->file('file')) { // Si se está enviando una imagen
            $url = Storage::put('posts', $request->file('file'));  //guardo la imagen en la carpeta post y copio su url en la variable $url

            $post->image()->create([ // al usar create para almacenar la imagen en la tabla img debo habilitar asignacion masiva en Image.php
                'url' => $url
            ]);
        }         

        if($request->tags)
        {
            $post->tags()->attach($request->tags);
        }

        return redirect()->route('admin.posts.index')->with('info', 'Se ha creado con exito el post: ' . $post->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::pluck('name', 'id'); //Devuelve un array de parejas clave=>valor ('id'=>'nombre')
        $tags = Tag::all();//Devuelve un array de objetos Tag.

        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\PostRequest  $request
     * @param  App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $post->update($request->all()); // actualizo los datos del post

        if($request->file('file')){ //Si se envia una imagen en el formulario
            $url = Storage::put('posts', $request->file('file'));  //la almaceno en la carpeta posts
            if($post->image){ //si el registro del post tiene una imagen previa asociada
                Storage::delete([$post->image->url]); //borro la imagen de la carpeta posts
                $post->image->update([  // Y cambio la url por la nueva
                    'url' => $url
                ]);
            }else{ // Si no tiene imagen previa le asigno la url de la nueva
                $post->image()->create([
                    'url' => $url
                ]);
            }
        }

        if($request->tags){            
            $post->tags()->sync($request->tags); // El método sync sincroniza las etiquetas, eliminando las desseleccionadas y agregando las nuevas.
        }

        return redirect()->route('admin.posts.edit', $post)->with('info', 'El post se actualizó con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('admin.posts.index')->with('info-del', 'El post se eliminó con éxito');
    }
}

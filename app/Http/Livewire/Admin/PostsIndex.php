<?php

namespace App\Http\Livewire\Admin;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;


class PostsIndex extends Component
{
    use WithPagination;  //para usar la paginación con livwwire

    protected $paginationTheme = "bootstrap"; //para que livewire use los estilos de bootstrap en vez de tailwind
    
    public $search;

    public function updatingSearch() // Solo se activa cuando pa propiedad search cambia de valor
    {
        $this->resetPage();
    }

    public function render()
    {
        $posts = Post::where('user_id', auth()->user()->id)
                        ->where('name', 'LIKE', '%' . $this->search . '%')
                        ->latest('id')
                        ->paginate();
        
        return view('livewire.admin.posts-index', compact('posts'));
    }
}

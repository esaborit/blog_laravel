<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class UsersIndex extends Component
{
    use WithPagination;  //para usar la paginación con livwwire

    protected $paginationTheme = "bootstrap"; //para que livewire use los estilos de bootstrap en vez de tailwind
    
    public $search;

    public function updatingSearch() // Solo se activa cuando la propiedad search cambia de valor
    {
        $this->resetPage();
    }
    
    public function render()
    {
        $users = User::where('name', 'LIKE', '%' . $this->search . '%')
                        ->orwhere('email', 'LIKE', '%' . $this->search . '%')
                        ->paginate();

        return view('livewire.admin.users-index', compact('users'));
    }
}

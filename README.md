
# Crear un proyecto laravel desde este repositorio

## Clonación del proyecto

Me conecto a mi servidor mediante ssh y me sitúo en la carpeta raíz de mis proyectos laravel. Una vez allí utilizo git para clonar el repositorio

```bash
enrique@enrique-server:/var/www/laravel$ git clone https://gitlab.com/esaborit/blog_laravel.git
```

Una vez que termina la clonación abro el proyecto en Visual Studio Code.

## Instalacion de dependencias

En el proyecto recién clonado no aparecen ni la carpeta `vendors`  (dependencias de php que necesita laravel para funcionar) ni la carpeta `node_modules` (dependencias de npm) ni el archivo `.env` .

Tengo que generar dichas carpetas desde la consola ejecutando el siguiente comando desde dentro de la carpeta principal del proyecto

```bash
enrique@enrique-server:/var/www/laravel/blog$ composer install
```

El comando anterior me instala las dependencias de `php` y me genera la carpeta `vendor`.

Lo siguiente es instalar todas las dependencias de `npm` .

```bash
enrique@enrique-server:/var/www/laravel/blog$ npm install
```

Continuo creando el archivo `.env` a partir del archivo `.env.example`

Una vez copiado lo debo editar poniendo la `APP_URL` de mi proyecto, el nombre de a base de datos del proyecto en la variable `DB_DATABASE`,  el usuario `DB_USERNAME` y la password `DB_PASSWORD`.

Para generar la `APP_KEY` debo ejecutar el siguiente comando desde la terminal.

```bash
enrique@enrique-server:/var/www/laravel/blog$ php artisan key:generate
```

Hecho esto el contenido del archivo quedaría como sigue.

```bash
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:l+Nh2C7bluemnxlKzZl2dcJ1qbtWctJdI3M/2bd4t47g=
APP_DEBUG=true
APP_URL=http://blog.test

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=blog
DB_USERNAME=el_usuario
DB_PASSWORD=la_password

```

Falta por generar el acceso directo desde la carpeta `public` a la carpeta `storage` . Lo hago desde la consola con el siguiente comando:

```bash
enrique@enrique-server:/var/www/laravel/blog$ php artisan storage:link
```

## Generación de registros

Por último genero todos los registros de prueba.

```bash
enrique@enrique-server:/var/www/laravel/blog$ php artisan migrate:fresh --seed
```
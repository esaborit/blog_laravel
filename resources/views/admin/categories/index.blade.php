@extends('adminlte::page')

@section('title', 'Blog con Laravel')

@section('content_header')
    <h1>Lista de categorías.</h1>
@stop

@section('content')
    <p>Aqui aparecerán las categorías</p>
    @if (session('info'))
        <div class=" alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @elseif(session('info-del'))
        <div class=" alert alert-danger">
            <strong>{{session('info-del')}}</strong>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <a class=" btn btn-secondary float-right" href="{{route('admin.categories.create')}}">Crear Categoría</a>
        </div>
        <div class="card-body">

            <table class=" table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <th>{{$category->id}}</th>
                        <th>{{$category->name}}</th>
                        <th width="10px"><a class=" btn btn-primary btn-sm" href="{{route('admin.categories.edit', $category)}}">Editar</a></th>
                        <th width="10px">
                            <form action="{{route('admin.categories.destroy', $category)}}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                            </form>
                        </th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@extends('adminlte::page')

@section('title', 'Blog con Laravel')

@section('content_header')
    <h1>Listado etiquetas</h1>
@stop

@section('content')
    <p>Mostrar etiquetas.</p>
    @if (session('info'))
        <div class=" alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @elseif(session('info-del'))
        <div class=" alert alert-danger">
            <strong>{{session('info-del')}}</strong>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <a class=" btn btn-secondary float-right" href="{{route('admin.tags.create')}}">Crear etiqueta</a>
        </div>
        <div class="card-body">
            <table class=" table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Color</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <th>{{$tag->id}}</th>
                            <th>{{$tag->name}}</th>
                            <th>{{$tag->color}}</th>
                            <th width="10px"><a class=" btn btn-primary btn-sm" href="{{route('admin.tags.edit', $tag)}}">Editar</a></th>
                            <th width="10px">
                                <form action="{{route('admin.tags.destroy', $tag)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>



@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
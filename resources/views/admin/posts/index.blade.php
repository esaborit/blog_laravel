@extends('adminlte::page')

@section('title', 'Blog con Laravel')

@section('content_header')
    <a class="btn btn-secondary btn-m float-right" href="{{route('admin.posts.create')}}">Crear nuevo post</a>
    <h1>Listado de posts</h1>
@stop

@section('content')
    @if (session('info'))
        <div class=" alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @elseif(session('info-del'))
        <div class=" alert alert-danger">
            <strong>{{session('info-del')}}</strong>
        </div>
    @endif
    @livewire('admin.posts-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
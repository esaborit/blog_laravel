<x-app-layout>
    <div class=" micontainer py-8">
        <h1 class=" text-4xl font-bold text-gray-600">{{$post->name}}</h1>
        <div class=" text-lg text-gray-500 mb-2">
            {!!$post->extract!!} {{-- Sustituido por {{$post->extract}} para que laravel no escape el html usado con ckeditor--}}
        </div>
        <div class="grid grid-cols-1 lg:grid-cols-3 gap-6">
            {{-- Contenido principal --}}
            <div class=" lg:col-span-2">
                <figure>
                   @if ($post->image)
                        <img class=" w-full h-80 object-cover object-center" src="{{Storage::url($post->image->url)}}" alt="">
                   @else
                        <img class=" w-full h-80 object-cover object-center" src="https://cdn.pixabay.com/photo/2016/10/22/17/46/mountains-1761292_960_720.jpg" alt="">
                   @endif
                </figure>
                <div class=" text-base text-gray-500 mt-4">
                    {!!$post->body!!} {{-- Sustituido por {{$post->body}} para que laravel no escape el html usado con ckeditor--}}
                </div>
            </div>
            {{-- Contenido relacionado--}}
            <aside>
                <h1 class=" text-2xl font-bold text-gray-600 mb-4">Más en: {{$post->category->name}}</h1>
                <ul>
                    @foreach ($similares as $similar)
                        <li class=" mb-4">
                            <a class="flex" href="{{Route('posts.show', $similar)}}">
                                <img class=" w-40 h-20 object-cover object-center " src="@if($similar->image) {{Storage::url($similar->image->url)}} @else https://cdn.pixabay.com/photo/2016/10/22/17/46/mountains-1761292_960_720.jpg @endif" alt="">
                                <span class=" ml-2 text-gray-600">{{$similar->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </aside>
        </div>
    </div>
</x-app-layout>
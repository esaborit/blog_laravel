<div class=" card">
    <div class="card-header">
        {{-- Sincronizo el input con la propiedad search de PostIndex.php--}}
        <input wire:model = "search" class=" form-control" placeholder="Escriba el nombre de un post">
    </div>
    @if ($posts->count())
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <th>{{$post->id}}</th>
                            <th>{{$post->name}}</th>
                            <th width="10px"><a class=" btn btn-primary btn-sm" href="{{route('admin.posts.edit', $post)}}">Editar</a></th>
                            <th width="10px">
                                <form action="{{route('admin.posts.destroy', $post)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger btn-sm" type="submit">Eliminar</button>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- Aqui pongo la paginación--}}
        <div class="car-footer">
            {{$posts->links()}}
        </div>
    @else
        <div class="card-body">
            <strong>No hay ninguna coincidencia.</strong>
        </div>
    @endif
</div>
